/**
 * 
 */
package com.acesoft.textit.free;
import java.util.ArrayList;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Data;
import android.util.Log;
//import android.provider.ContactsContract.Data;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
//import android.content.ContentResolver;
//import android.content.ContentProviderResult;
import com.acesoft.textit.free.R;

public class MultipleSelectionAdapter extends BaseAdapter {

	public ArrayList<String> mName;
	public ArrayList<Boolean> mCheck;
	public ArrayList<String> mId;
	public ArrayList<String> mNumber;
	public ArrayList<Bitmap> mTn;
	public Integer nRec = 0;
	//public Integer nSelected = 0;
	protected TextView gtv;
	protected Activity thisact;
	protected String[] dataProjection = new String[] {
			Data.MIMETYPE,
			Data.DATA1,
			Data.DATA2,
	};
	protected Cursor dataCursor;
	protected String RawID;
	protected String TAG = "Test";
	protected Integer nNumber;
	LayoutInflater mInflater;

	public MultipleSelectionAdapter(Activity act, TextView ptv, ArrayList<String> mName,
			ArrayList<Boolean> mCheck, ArrayList<String> mId, ArrayList<String> mNumber,
			ArrayList<Bitmap> mTn) {
		this.mName = mName;
		this.mCheck = mCheck;
		this.mId = mId;
		this.mNumber = mNumber;
		this.mTn = mTn;
		gtv = ptv;
		thisact = act;
		mInflater = LayoutInflater.from(act);
	}

	@Override
	public int getCount() {
		return mName.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	private class DataHolder {
		CheckBox mCheckBox;
		TextView mtextName;
		ImageView mphotoTn;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final DataHolder mHolder;
		// TextView tv = (TextView) convertView.findViewById(R.id.NumberOfRecipients); 
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.activity_rowview, null);
			mHolder = new DataHolder();
			mHolder.mCheckBox = (CheckBox) convertView
					.findViewById(R.id.contactEntrycHECK);
			mHolder.mtextName = (TextView) convertView
					.findViewById(R.id.contactEntryText);
			mHolder.mphotoTn = (ImageView) convertView
					.findViewById(R.id.contactEntryPhoto);
			convertView.setTag(mHolder);
		} else {
			mHolder = (DataHolder) convertView.getTag();
		}
		mHolder.mCheckBox
			.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
		  @Override
		  public void onCheckedChanged(CompoundButton buttonView,
			boolean isChecked) {
			mCheck.set(position, isChecked);
			MainActivity.dlSaved = false;
			if (isChecked) {
			  // TextView tv =(TextView) buttonView.findViewById(R.id.NumberOfRecipients); 
			  RawID = mId.get(position);
			  dataCursor = thisact.getContentResolver().query(ContactsContract.Data.CONTENT_URI, dataProjection, 
	        		Data.RAW_CONTACT_ID + "=? AND " + 
	        		ContactsContract.Data.MIMETYPE + " = ? AND " +
					ContactsContract.Data.DATA2 + " = ?",
	        		new String[] {RawID, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
					  Integer.toString(ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)},
			//		ContactsContract.Data.MIMETYPE + " = ?",
			//		new String[] {RawID, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE},
	        		null);
			  nNumber = dataCursor.getCount();
			  //Log.e(TAG, "Cursor size: " + nNumber);
			  if (nNumber == 0) {
				sDialog("No Mobile Number for " + mName.get(position) + 
					". Add a Mobile Number to this conact first before selecting.");
				mCheck.set(position, false);
				mHolder.mCheckBox.setChecked(false);
			  } else if (nNumber > 1) {
				if (mNumber.get(position).equals(" ")) {
					final String mobileNumbers[] = new String[nNumber];
					dataCursor.moveToFirst();
					for (int i=0; i<nNumber; i++) {
						mobileNumbers[i] = dataCursor.getString(1);
						dataCursor.moveToNext();
					}
					AlertDialog.Builder builder = new AlertDialog.Builder(thisact);    
					builder.setTitle("Pick 1 Mobile Number:")           
				  		.setItems(mobileNumbers, new DialogInterface.OnClickListener() {               
				  			public void onClick(DialogInterface dialog, int which) {               
				  				mNumber.set(position, mobileNumbers[which]);
				  				//Log.e(TAG, "Number picked: " + mobileNumbers[which]);      
				  			}    
				  		});
					dataCursor.close();
					AlertDialog selectNumber = builder.create();				  
					selectNumber.show();
					//nSelected = nSelected + 1;
					//gtv.setText("Number of Recipients selected: " + nSelected);
					//if (mNumber.get(position).equals(" ")) {
					//	mCheck.set(position, false);
					//	mHolder.mCheckBox.setChecked(false);
					//} else {
						updNumberRecipients();
					//}
				} else {    // mNumber at this position is not blank
					updNumberRecipients();
				}
			  } else {      // nNumber == 1
				dataCursor.moveToFirst();  
				mNumber.set(position, dataCursor.getString(1));
				dataCursor.close();
				//nSelected = nSelected + 1;
				//gtv.setText("Number of Recipients selected: " + nSelected);
				updNumberRecipients();
			  }			  
			} else {
			  //nSelected = nSelected - 1;	
			  //gtv.setText("Number of Recipients selected: " + nSelected);
			  updNumberRecipients();
			}
				
			}
		  });
		mHolder.mtextName.setText(mName.get(position));
		mHolder.mCheckBox.setChecked(mCheck.get(position));
		mHolder.mphotoTn.setImageBitmap(mTn.get(position));

		return convertView;
	}
	public void sDialog(String dMessage) {
		Context thiscontext = thisact;
		AlertDialog.Builder alertdialog = new AlertDialog.Builder(thiscontext);
		 alertdialog.setTitle("Information");
		 alertdialog.setMessage(dMessage);
		 alertdialog.setCancelable(false);
		 alertdialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				// finish();
			}
			});
		 AlertDialog dialogbox = alertdialog.create();
		 dialogbox.show();
	}
	public void updNumberRecipients() {
		ArrayList<Boolean> mChBooleans = mCheck;
		nRec = 0;
		for (boolean check : mChBooleans) {
			if (check) {
				nRec = nRec + 1;
			}
		}
		gtv.setText(MainActivity.sdl + " - Recipients selected: " + nRec);
	}
	
}