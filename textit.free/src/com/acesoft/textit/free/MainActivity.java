package com.acesoft.textit.free;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.view.Menu;
import android.app.ListActivity;

import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.Policy;
import com.google.android.vending.licensing.ServerManagedPolicy;
/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.provider.Settings.Secure;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.acesoft.textit.free.R;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;

public final class MainActivity extends Activity implements OnClickListener, OnItemSelectedListener, OnItemClickListener {

	public static final String TAG = "ContactManager";

	private ListView mContactList, smsList;
	private boolean mShowInvisible = false;

	static boolean dlSaved = false;
	static int screenNo = 1;
	static String sdl, osdl;
	protected TextView tv;
	protected Integer dnIndex, idIndex, genIndex, addrIndex, dateIndex, typeIndex, bodyIndex;
	//protected String DLFileName;
	protected ArrayList<String> sRecipients = new ArrayList<String>();
	protected ArrayList<String> sNumbers = new ArrayList<String>();
	protected Spinner selectDL;
	protected String[] distLine;
	protected ViewFlipper vf;
	protected ArrayAdapter<String> dataAdapter;
	protected String distributions[];
	protected SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	protected ArrayList<String> mline1;
	protected ArrayList<String> mline2;
	protected ArrayList<String> mtype;
	protected Cursor cursor;
	protected ArrayList<String> mNamList = new ArrayList<String>();
	protected ArrayList<Boolean> mCheckList = new ArrayList<Boolean>();
	protected ArrayList<String> mIdList = new ArrayList<String>();
	protected ArrayList<String> mNumberList = new ArrayList<String>();
	protected ArrayList<Bitmap> mTnList = new ArrayList<Bitmap>();
	protected Bitmap photoBitmap;
	protected String RawID;
	protected MultipleSelectionAdapter mUAdapter;
		
	/**
	 Version 1.5, June 10, 2013 - Use Options 2 to reduce default image size. Set activity 
	 orientation to Portrait permanently. 
	*/
	@Override
	public void onCreate(Bundle savedInstanceState) {
		//Log.v(TAG, "Activity State: onCreate()");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		sdl = "Untitled";
		//Log.e(TAG, "OnCreate started");
		// Obtain handles to UI objects
		mContactList = (ListView) findViewById(R.id.contactList);
		smsList = (ListView) findViewById(R.id.listSMS);
		// smsList = (ListView) findViewById(R.id.listSMS);
		findViewById(R.id.btn_Save).setOnClickListener(this);
		findViewById(R.id.btn_SendSMS).setOnClickListener(this);
		findViewById(R.id.btn_SaveAs).setOnClickListener(this);
		tv = (TextView) findViewById(R.id.SelectDist);
		vf = (ViewFlipper) findViewById( R.id.viewFlipper );

		// Populate the contact list
		populateContactList(false);
				
	}

	/**
	 * Populate the contact list based on account currently selected in the
	 * account spinner.
	 */
	private void populateContactList(boolean updOnly) {
		cursor = getContacts();
		//mNamList = new ArrayList<String>();
		//mCheckList = new ArrayList<Boolean>();
		//mIdList = new ArrayList<String>();
		//mNumberList = new ArrayList<String>();
		//cursor.moveToFirst();
		dnIndex = cursor.getColumnIndex(RawContacts.DISPLAY_NAME_PRIMARY);
		idIndex = cursor.getColumnIndex(RawContacts._ID);
		mNamList.clear();
		mIdList.clear();
		mCheckList.clear();
		mNumberList.clear();
		mTnList.clear();
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 2; // might try 8 also
		
		while (cursor.moveToNext()) {
			mNamList.add(cursor.getString(dnIndex));			
			RawID = cursor.getString(idIndex);
			mIdList.add(RawID);
			//		.getColumnIndex(ContactsContract.Data._ID)));
			mCheckList.add(false);
			mNumberList.add(" ");
			
			photoBitmap = null;
			String[] TNprojection = new String[] {ContactsContract.CommonDataKinds.Photo.DATA15};
			Cursor TNcursor = getContentResolver().query(
					ContactsContract.Data.CONTENT_URI, TNprojection, 
			        Data.RAW_CONTACT_ID + "=? AND " + 
			        ContactsContract.Data.MIMETYPE + " = ?", 
			        new String[] {RawID, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE}, 
			        null);
			if ((TNcursor != null) && (TNcursor.moveToFirst())) {	
				byte[] photoBlob = TNcursor.getBlob(0);
				if (photoBlob != null) {
					//BitmapFactory.Options options = new BitmapFactory.Options();
					//options.inSampleSize = 2; // might try 8 also
					photoBitmap = BitmapFactory.decodeByteArray(photoBlob, 0, photoBlob.length, options);
				}
				TNcursor.close();
			}
			if (photoBitmap == null) {
				photoBitmap = BitmapFactory.decodeResource(getResources(), 
						android.R.drawable.ic_menu_camera, options);
			}
			mTnList.add(photoBitmap);			
		}
		//Log.e(TAG, "Updating contact listview. Cursor count: " + cursor.getCount());
		cursor.close();
		if (updOnly) {
			mUAdapter.notifyDataSetChanged();
		} else {
			mUAdapter = new MultipleSelectionAdapter(this, tv, mNamList, mCheckList, 
					mIdList, mNumberList, mTnList);
			mContactList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
			mContactList.setAdapter(mUAdapter);
		}
	}

	//MultipleSelectionAdapter mUAdapter;

	/**
	 * Obtains the contact list for the currently selected account.
	 * 
	 * @return A cursor for for accessing the contact list.
	 */
	private Cursor getContacts() {
/*
		Uri uri = ContactsContract.Contacts.CONTENT_URI;
		String[] projection = new String[] { ContactsContract.Contacts.,
				ContactsContract.Contacts.DISPLAY_NAME };
		String selection = ContactsContract.Contacts.IN_VISIBLE_GROUP + " = '"
				+ (mShowInvisible ? "0" : "1") + "'";
		String[] selectionArgs = null;
		String sortOrder = ContactsContract.Contacts.DISPLAY_NAME
				+ " COLLATE LOCALIZED ASC";

		return managedQuery(uri, projection, selection, selectionArgs,
				sortOrder);
*/
		String[] projection = new String[] {RawContacts._ID, RawContacts.DISPLAY_NAME_PRIMARY};
		String sortOrder = RawContacts.DISPLAY_NAME_PRIMARY
				+ " COLLATE LOCALIZED ASC";
		return getContentResolver().query(
				RawContacts.CONTENT_URI, 
				projection,
				RawContacts.DELETED + "=?", 
				new String[] {"0"},             // Get the list of undeleted contacts.
//				null, null,
				sortOrder);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_SaveAs:
			//Log.e(TAG, "SaveAS executed");
			if (mUAdapter.nRec > 0) {
				if (mUAdapter.nRec < 4) {  // Free version max 3
					PromptDialog dlg = new PromptDialog(this, R.string.enter_comment, R.string.title) {
						@Override
						public boolean onOkClicked(String input) {
							//Log.e(TAG, "String entered: " + input);  // do something
							CreateDL(input);
							return true; // true = close dialog
						}
					};
					dlg.show();
				} else {  // more than 3
					Toast.makeText(this, "Free version allows maximum 3 recipients.", Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(this, "Nothing to save! ", Toast.LENGTH_SHORT).show();
			}
			break;
			
		case R.id.btn_Save:
			if (mUAdapter.nRec > 0) {
				if (mUAdapter.nRec < 4) {  // max 3 for free version
					CreateDL(sdl);
				} else {  // more than 3
					Toast.makeText(this, "Free version allows maximum 3 recipients.", Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(this, "Nothing to save! ", Toast.LENGTH_SHORT).show();
			}
			break;
			
		case R.id.btn_SendSMS:
			if (mUAdapter.nRec < 4) {
				if (!dlSaved && mUAdapter.nRec > 0) {
					CreateDL(sdl);
				}
				screenNo = 2;
				HandleSMS();				
			} else {
				Toast.makeText(this, "Free version allows maximum 3 recipients.", Toast.LENGTH_LONG).show();
			}
			break;	
		}
		//screenNo = 2;
	}
	
	public void CreateDL(String DLName) {
		osdl = sdl;
		sdl = DLName.replaceAll(" ", "_");
		int i, j;
		i = 0;
		j = 0;
		try {
			FileOutputStream fos = openFileOutput(sdl, this.MODE_PRIVATE);
			OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-16");
			ArrayList<Boolean> mChBooleans = mUAdapter.mCheck;
			for (boolean check : mChBooleans) {
				if (check) {
					osw.write(mUAdapter.mName.get(i) + " -- " + mUAdapter.mNumber.get(i) + "\r");
					j = j + 1;
				}
				i++;
			}
			osw.flush();
			osw.close();
		} catch (Exception e) {
			Log.e(TAG, "Exception in writing DL File.");
		}
		distributions = fileList();
		if (distributions.length > 2) {
			File dir = getFilesDir();
			File thisfile = new File(dir, sdl);
			thisfile.delete();
			Toast.makeText(this, "Free version allows maximum 2 D.Lists. D.List not saved.", Toast.LENGTH_LONG).show();
			sdl = osdl;
		} else {
			Toast.makeText(this, "Distribution List, " + DLName + ", saved.", Toast.LENGTH_SHORT).show();
			dlSaved = true;
			tv.setText(sdl + " - Recipients selected: " + j);
		}
	}
	
	public void HandleSMS() {
		//setContentView(R.layout.sendtext);
		vf.showNext();
		distributions = fileList();
		selectDL = (Spinner) findViewById(R.id.Spinner02);
		//List<String> list = new ArrayList<String>();
		//list.add("list 1");
		//list.add("list 2");
		//list.add("list 3");
		dataAdapter = new ArrayAdapter<String>(this,
			android.R.layout.simple_spinner_dropdown_item, distributions);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		selectDL.setAdapter(dataAdapter);
		genIndex = 0;
		for (int i=0; i<distributions.length; i++){
             if (distributions[i].equals(sdl)) {
            	 genIndex = i;
             }
        }		
		selectDL.setSelection(genIndex);
		selectDL.setOnItemSelectedListener(this);		
	}

	/* (non-Javadoc)
	 * @see android.widget.AdapterView.OnItemSelectedListener#onItemSelected(android.widget.AdapterView, android.view.View, int, long)
	 */
	@Override
	public void onItemSelected(AdapterView<?> av, View vw, int i, long l) {
		// TODO Auto-generated method stub
		sdl = (String) selectDL.getSelectedItem();
		//Log.e(TAG, "OnItemSelected changed sdl");
		sRecipients.clear();
		sNumbers.clear();
		TextView recip = (TextView) findViewById(R.id.SpinnerResult);
		String total = "";
		try{
	        FileInputStream fIn = openFileInput(sdl);
	        BufferedReader br = new BufferedReader(new InputStreamReader(fIn, "UTF-16"));
	        
	        String recipient = "";
	        recipient = br.readLine();

	        while(recipient!= null){
	        	//Log.e(TAG, "Readline: " + recipient);
	        	distLine = recipient.split(Pattern.quote(" -- "));
	        	sRecipients.add(distLine[0]);
	        	sNumbers.add(distLine[1]);
	        	total = total + "\r\n" + recipient;
	        	recipient = br.readLine();
	        }
	        fIn.close();
	    }
	    catch(IOException e){
	    	Log.e(TAG, "Exception in reading Distribution List");
	    	// TODO: handle exception
	    }
		//((TextView) av.getChildAt(0)).setTextColor(Color.BLUE);
        //((TextView) av.getChildAt(0)).setTextSize(20);

		recip.setMovementMethod(new ScrollingMovementMethod());
		recip.setText(total);
		recip.scrollTo (0, 0);
		
	}

	/* (non-Javadoc)
	 * @see android.widget.AdapterView.OnItemSelectedListener#onNothingSelected(android.widget.AdapterView)
	 */
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub		
	}
	
	public void EditDL(View v) {

		//setContentView(R.layout.activity_main);
		vf.showPrevious();
		//mContactList = (ListView) findViewById(R.id.contactList);
		// populateContactList();
		ArrayList<String> mStrName = mUAdapter.mName;
		screenNo = 1;
		int i = 0;
		for (String rname : mStrName) {
			genIndex = sRecipients.indexOf(rname);
			if (genIndex == -1) {
				mUAdapter.mCheck.set(i, false);
			} else {					
				mUAdapter.mCheck.set(i, true);
				mUAdapter.mNumber.set(i, sNumbers.get(genIndex));
			}
			i++;
		}
		tv.setText(sdl + " - Recipients selected: " + sNumbers.size());
		mUAdapter.nRec = sNumbers.size();
		mUAdapter.notifyDataSetChanged();
	}
	
	public void DeleteDL(View v) {
		
		File dir = getFilesDir();
		File thisfile = new File(dir, sdl);
		thisfile.delete();
		//Log.e(TAG, "Deleted: " + sdl);
		vf.showPrevious();
		HandleSMS();
		Toast.makeText(this, sdl + " Distribution List deleted. ", Toast.LENGTH_SHORT).show();
	}
	
	public void NewDL(View v) {
		vf.showPrevious();
		// mContactList = (ListView) findViewById(R.id.contactList);
		// populateContactList();
		ArrayList<Boolean> mCheckItems = mUAdapter.mCheck;
		screenNo = 1;
		int i = 0;
		for (boolean thisCheck : mCheckItems) {
			mUAdapter.mCheck.set(i, false);
			mContactList.setItemChecked(i, false);
			i = i + 1;
		}
		sdl = "Untitled";
		tv.setText(sdl + " - Recipients selected: 0");
		
	}
	
	public void ForSMS (View v) {
		//setContentView(R.layout.selectsms);
		vf.showNext();
		//smsList = (ListView) findViewById(R.id.listSMS);
		Cursor smscursor; 
		Uri smsuri = Uri.parse("content://sms/"); 
		String[] smsprojection = new String[] { "address", "date", "type", "body"};
		String smssortorder = "date desc";		
		smscursor = getContentResolver().query(smsuri, smsprojection, null, null, smssortorder);
		screenNo = 3;
		mline1 = new ArrayList<String>();
		mline2 = new ArrayList<String>();
		mtype = new ArrayList<String>();
		smscursor.moveToFirst();
		addrIndex = smscursor.getColumnIndex("address");
		dateIndex = smscursor.getColumnIndex("date");
		typeIndex = smscursor.getColumnIndex("type");
		bodyIndex = smscursor.getColumnIndex("body");
		//Log.e(TAG, "Size of smscursor: " + smscursor.getCount());
		//Log.e(TAG, "Indexes: " + addrIndex + dateIndex + typeIndex + bodyIndex);
	
		while (smscursor.moveToNext()) {
			//Log.e(TAG, "Address: " + smscursor.getString(addrIndex));
			mline1.add(formatline1(smscursor.getString(addrIndex), 
					smscursor.getString(dateIndex), smscursor.getString(typeIndex)));			
			mline2.add(smscursor.getString(bodyIndex));
			mtype.add(smscursor.getString(typeIndex));
		}
		smsAdapter = new smsArrayAdapter(this, mline1, mline2);
		smsList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		smsList.setAdapter(smsAdapter);
	
		smsList.setOnItemClickListener(this);
		
	}
	
	smsArrayAdapter smsAdapter;
	
	public String formatline1(String addr, String dt, String ty) {
		String FromTo;
		if (addr != null) {
			genIndex = addr.length();
			if (genIndex > 16) {
				addr = addr.substring(0, 15) + "..  ";
			} else {
				for (int i=genIndex; i<20; i++) {
					addr = addr + " ";
				}
			}
		} else {
			addr = "                      ";
		}
		if (ty.equals("1")) {
			FromTo = "From: ";
		} else if (ty.equals("2")) {
			FromTo = "To: ";
		} else {
			FromTo = "Draft: ";
		}
		String sdt = formatter.format(Long.valueOf(dt));
		sdt = sdt.substring(5);
		return (FromTo + addr + sdt);		
	}
	
	@Override
    public void onBackPressed() {
		//super.onBackPressed();
		//Log.e(TAG, "screenNo: " + screenNo);
		switch (screenNo) {
		case 1:
			Toast.makeText(this, "Exit", Toast.LENGTH_SHORT).show();
			finish();
			break;
		case 2:
			View cview = this.findViewById(android.R.id.content);
			NewDL(cview);
			break;
		case 3:
			vf.showPrevious();
			screenNo = 2;
			break;		
		}
	}

	@Override
	public void onDetachedFromWindow() {
		try {
			super.onDetachedFromWindow();
		}
		catch (Exception e) {
			// Call stopFlipping() in order to kick off updateRunning()
			// stopFlipping();
		}
	}
	
	/* (non-Javadoc)
	 * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView, android.view.View, int, long)
	 */
	@Override
	public void onItemClick(AdapterView<?> pView, View cView, int position, long id) {
    	//Toast.makeText(this, mline2.get(position), Toast.LENGTH_SHORT).show();
		Log.e(TAG, mline2.get(position));
		int nrecip = sNumbers.size();
		if (nrecip > 0) {
			String phoneNos = sNumbers.get(0);
			for (int i=1; i<sNumbers.size(); i++) {
				phoneNos = phoneNos + ";" + sNumbers.get(i);
			}
			//Log.e(TAG, "# Recipients: " + sRecipients.size() + "  " + phoneNos);
			Intent smsIntent = new Intent(Intent.ACTION_VIEW);
			smsIntent.putExtra("sms_body", mline2.get(position));      
			smsIntent.putExtra("address", phoneNos);
			smsIntent.setType("vnd.android-dir/mms-sms");
			//if (accessAllowed) {
			startActivity(smsIntent);
			populateContactList(true);
			//} else {
			//	displayDialog(true);
			//}
		} else {
			Toast.makeText(this, "No recipients selected.", Toast.LENGTH_SHORT).show();
		}
	}
	
	public void NewSMS(View v) {
		int nrecip = sNumbers.size();
		if (nrecip > 0) {
			String phoneNos = sNumbers.get(0);
			for (int i=1; i<sNumbers.size(); i++) {
				phoneNos = phoneNos + ";" + sNumbers.get(i);
			}
			//Log.e(TAG, "# Recipients: " + sRecipients.size() + "  " + phoneNos);
			Intent smsIntent = new Intent(Intent.ACTION_VIEW);
			smsIntent.putExtra("sms_body", "");      
			smsIntent.putExtra("address", phoneNos);
			smsIntent.setType("vnd.android-dir/mms-sms");
			//if (accessAllowed) {
			startActivity(smsIntent);
			populateContactList(true);
			//} else {
			//	displayDialog(true);
			//}
		} else {
			Toast.makeText(this, "No recipients selected.", Toast.LENGTH_SHORT).show();
		}
	}
	public void BuyNow(View v) {
		Toast.makeText(this, "Require wifi or cellular data.", Toast.LENGTH_SHORT).show();
		Uri uri = Uri.parse("market://search?q=pname:com.acesoft.text.it");		
		startActivity(new Intent(Intent.ACTION_VIEW, uri));	
	}
}
