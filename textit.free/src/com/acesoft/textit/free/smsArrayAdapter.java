/**
 * 
 */
package com.acesoft.textit.free;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.acesoft.textit.free.R;
 
public class smsArrayAdapter extends BaseAdapter {

	public ArrayList<String> line1;
	public ArrayList<String> line2;
	LayoutInflater smsInflater;
 
	public smsArrayAdapter(Activity act, ArrayList<String> line1, ArrayList<String> line2) {
		//super(context, R.layout.sms_row, line1);
		
		this.line1 = line1;
		this.line2 = line2;
		smsInflater = LayoutInflater.from(act);
	}
	
	private class smsHolder {
		TextView mline1;
		TextView mline2;
	}
 
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final smsHolder msmsHolder; 
		if (convertView == null) {
			convertView = smsInflater.inflate(R.layout.sms_row, null);
			msmsHolder = new smsHolder();
			msmsHolder.mline1 = (TextView) convertView.findViewById(R.id.smsToFrom);
			msmsHolder.mline2 = (TextView) convertView.findViewById(R.id.smsText);
			convertView.setTag(msmsHolder);
		} else {
			msmsHolder = (smsHolder) convertView.getTag();			
		}
		msmsHolder.mline1.setText(line1.get(position));
		msmsHolder.mline2.setText(line2.get(position));
 
		return convertView;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return line1.size();
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}
