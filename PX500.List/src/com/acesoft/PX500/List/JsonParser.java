/**
 * 
 */
package com.acesoft.PX500.List;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

/**
 * @author AppDev
 *
 */
public class JsonParser {

	static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";
    public boolean getSuccess;
 
    // constructor
    public JsonParser() {
 
    }
 
    public JSONObject getJSONFromUrl(String url) {
 
        StrictMode.ThreadPolicy spolicy = new StrictMode.ThreadPolicy.Builder().build();
        StrictMode.setThreadPolicy(spolicy);
        getSuccess = true;
    	
    	// Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpPost = new HttpGet(url);
 
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();           
 
        } catch (UnsupportedEncodingException e) {
        	getSuccess = false;
        	e.printStackTrace();
        } catch (ClientProtocolException e) {
        	getSuccess = false;
        	e.printStackTrace();
        } catch (IOException e) {
        	getSuccess = false;
        	e.printStackTrace();
        } catch (Exception e) {
        	getSuccess = false;
        	e.printStackTrace();
        }
 
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
        	getSuccess = false;
        	Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        //Log.e("JSON String ", json);
        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
        	getSuccess = false;
        	Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
 
        // return JSON String
        return jObj;
 
    }

}
