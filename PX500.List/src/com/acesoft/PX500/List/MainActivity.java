package com.acesoft.PX500.List;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnScrollListener {

	protected String URLstr = "https://api.500px.com/v1/photos?feature=fresh_today&sort=popularity&image_size=3&include_store=store_download&include_states=voted&consumer_key=OPNnllOran0fvBzmenpMhafh7AH6AftjqSmNDQQH&page=";
	protected String jsonURL;
	protected String keystring, tstr;
	protected TextView tview, apiText;
	protected String TAG = "TestJ";
	protected static String[] descript = new String[21];
	protected String imgURL;
	protected Integer ides, iimg, ipos, iend, ipage;
	protected static Bitmap[] imgList = new Bitmap[21];
	protected Bitmap photoBitmap;
	protected BitmapFactory.Options options = new BitmapFactory.Options();
	protected static ListView list;
	public static ProgressDialog pd;
	public static JsonParser jParser = new JsonParser();
	public JSONObject jsonObj;
	protected static CustomList adapter;
	protected boolean dettop = false;
	protected boolean detbottom = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		 
        setContentView(R.layout.activity_main);
        apiText = (TextView) findViewById(R.id.apiTextview);
		apiText.setText("Scroll to see more!");
		pd = new ProgressDialog(this);
	    pd.setMessage("Loading. Please wait...");

	    ipage = 1;
	    jsonURL = URLstr + String.valueOf(ipage);
        jsonObj = jParser.getJSONFromUrl(jsonURL);
        
        options.inSampleSize = 2; // might try 8 also
        photoBitmap = BitmapFactory.decodeResource(getResources(), 
				android.R.drawable.ic_menu_camera, options);
        
        for (int i = 0; i < 21; i++) {
        	descript[i] = "Untitled";
        	imgList[i] = photoBitmap;
        }
        new TheTask().execute();  
		
		//Log.e("JSON Contents", jParser.json);	
		
		adapter = new
            CustomList(MainActivity.this, descript, imgList);
			list=(ListView)findViewById(R.id.imageList);
            list.setAdapter(adapter);
            list.setOnScrollListener(this);
/*          list.setOnClickListener(new AdapterView.OnItemClickListener() {
 
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                          int position, long id) {
                   Toast.makeText(MainActivity.this, "You Clicked at position "+ position, Toast.LENGTH_SHORT).show();
 
                }
            });

*/		
		
		//tview = (TextView) findViewById(R.id.textView1);
    	//tview.setText(keystring);
		
		
		//dispKeys();
 
	}  
/*	
	public void dispKeys() {
		
		Log.e(TAG, "Before JsonParser");
		 
        // Creating new JSON Parser
        JsonParser jParser = new JsonParser();
        
        Log.e(TAG, "After JsonParser");
 
        // Getting JSON from URL
        JSONObject json = jParser.getJSONFromUrl(jsonURL);
        
        Log.e(TAG, "After getJSONFromUrl");
        
        if (jParser.getSuccess) {
        	Iterator<String> iter = json.keys();
        	keystring = "";
        	while (iter.hasNext()) {
        		String jKey = iter.next();
        		keystring = keystring + jKey + "\r\n";
        	}
        	
        	tview = (TextView) findViewById(R.id.textView1);
        	tview.setText(keystring);
        } else {
        	Toast.makeText(this, "Invalid URL", Toast.LENGTH_LONG).show();
        }
	}
*/    
	
	@Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            int loadedItems = firstVisibleItem + visibleItemCount;
            if (detbottom && (loadedItems == totalItemCount)){
            	ipage = ipage + 1;
            	jsonURL = URLstr + String.valueOf(ipage);
                jsonObj = jParser.getJSONFromUrl(jsonURL);
            	new TheTask().execute();
            	dettop = false;
            	list.setSelection(0);
            	detbottom = true;
            }
            if (dettop && (list.getFirstVisiblePosition() == 0) && (ipage > 1)) {
            	ipage = ipage - 1;
            	jsonURL = URLstr + String.valueOf(ipage);
                jsonObj = jParser.getJSONFromUrl(jsonURL);
            	new TheTask().execute();
            	detbottom = false;
            	list.setSelection(20);
            	dettop = true;
            }
            if (list.getFirstVisiblePosition() == 5) {
            	dettop = true;
            	//Log.e("OnScroll ", "dettop set to true");
            }
            if (list.getFirstVisiblePosition() == 15) {
            	detbottom = true;
            	//Log.e("OnScroll ", "dettop set to true");
            }
    }


    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    
	public void exeEnterURL(View ccScreen) {
		PromptDialog dlg = new PromptDialog(this, R.string.dialog_head, R.string.dialog_title) {
			@Override
			public boolean onOkClicked(String input) {
				//Log.e(TAG, "String entered: " + input);  // do something
				jsonURL = input;
				//dispKeys();
				return true; // true = close dialog
			}
		};
		dlg.show();
		//dispKeys();
	}
	
    public void exeExit(View ccScreen) {
    	finish();
    }
		

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}

class TheTask extends AsyncTask<Void,Void,Void>
{
	protected Bitmap image = null;
	protected Integer ipos, iend, iimg;
	protected String tstr, imgURL;
	
    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        MainActivity.pd.show();
    }


    @Override
    protected Void doInBackground(Void... params) {
        // TODO Auto-generated method stub
        try
        {     	
        	ipos = 0;
    		iimg = 0;
        	while (MainActivity.jParser.json.indexOf("\"name", ipos) > 0) {
    			ipos = MainActivity.jParser.json.indexOf("\"name", ipos);
    			iend = MainActivity.jParser.json.indexOf("\"description", ipos);
    			tstr = MainActivity.jParser.json.substring(ipos+8, iend-2);
    			if (!tstr.equals("ul")) {
    				MainActivity.descript[iimg] = tstr;
    			}			
    			ipos = MainActivity.jParser.json.indexOf("image_url", iend);
    			iend = MainActivity.jParser.json.indexOf("images", ipos);
    			imgURL = MainActivity.jParser.json.substring(ipos+12, iend-3);
    			MainActivity.imgList[iimg] = downloadBitmap(imgURL);
    			iimg = iimg + 1;
    		}
       
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        MainActivity.pd.dismiss();
        //MainActivity.list=(ListView)findViewById(R.id.imageList);
        //MainActivity.list.setAdapter(MainActivity.adapter);
/*        if(image!=null)
        {
            iv.setImageBitmap(image);
        }
*/
    } 
    
    private Bitmap downloadBitmap(String url) {
	     // initilize the default HTTP client object
	     final DefaultHttpClient client = new DefaultHttpClient();
	     
	     //forming a HttoGet request 
	     final HttpGet getRequest = new HttpGet(url);
	     try {

	         HttpResponse response = client.execute(getRequest);

	         //check 200 OK for success
	         final int statusCode = response.getStatusLine().getStatusCode();

	         if (statusCode != HttpStatus.SC_OK) {
	             Log.w("ImageDownloader", "Error " + statusCode + 
	                     " while retrieving bitmap from " + url);
	             return null;
	         }

	         final HttpEntity entity = response.getEntity();
	         if (entity != null) {
	             InputStream inputStream = null;
	             try {
	                 // getting contents from the stream 
	                 inputStream = entity.getContent();

	                 // decoding stream data back into image Bitmap that android understands
	                 image = BitmapFactory.decodeStream(inputStream);


	             } finally {
	                 if (inputStream != null) {
	                     inputStream.close();
	                 }
	                 entity.consumeContent();
	             }
	         }
	     } catch (Exception e) {
	         // You Could provide a more explicit error message for IOException
	         getRequest.abort();
	         Log.e("ImageDownloader", "Something went wrong while" +
	                 " retrieving bitmap from " + url + e.toString());
	     } 

	     return image;
	}
    
}


