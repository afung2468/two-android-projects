/**
 * 
 */
package com.acesoft.PX500.List;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author AppDev
 *
 */
public class CustomList extends ArrayAdapter<String>{	 
private final Activity context;
private final String[] web;
private final Bitmap[] images;

public CustomList(Activity context, String[] web, Bitmap[] images) {
super(context, R.layout.single_row, web);
this.context = context;
this.web = web;
this.images = images;
 
}

@Override
public View getView(int position, View view, ViewGroup parent) {
LayoutInflater inflater = context.getLayoutInflater();
View rowView= inflater.inflate(R.layout.single_row, null, true);
TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
 
ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
txtTitle.setText(web[position]);
 
imageView.setImageBitmap(images[position]);
return rowView;
}
}

